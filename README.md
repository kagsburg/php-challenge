
## About Project 


The project is to provide a RESTful API for a Task Management System. The API is built using Laravel 9 and is a RESTful API. The Trial folder contains the solutions for the questions 1-3.

The TaskApi folder contains the solution question 4.


## Installation

1. Clone the repository
2. Run `composer install`
3. Run `cp .env.example .env`
4. Run `php artisan key:generate`
5. Run `php artisan migrate --seed`
6. Run `php artisan serve`

## Usage

The API has the following endpoints:

- `POST /api/v1/register` - Register a new user
- `GET /api/v1/users` - Get all users
- `POST api/v1/signin` - Sign in a user
- `POST /api/v1/projects` - Create a new Project
- `GET /api/v1/projects` - Get all projects
- `GET /api/v1/projects/user/{id}` - Get a List of Projects Assigned to Specific Engineer
- `PATCH /api/v1/projects/{id}` - Update a project
- `GET /api/v1/roles` - Get all roles
- `PUT /api/v1/projects/changeMembers/{id}` - Update a Change Project Members
- `POST /api/v1/milestones/{id}` - Create a Milestone for a Project
- `GET /api/v1/milestones/{id}` - Get all Milestones for a Project
- `GET /api/v1/milestones` - Get all Milestones

I also Included a User Interface For the three Questions which is include in the Laravel App.


## POSTMAN LINK TO API DOCUMENTATION

https://documenter.getpostman.com/view/16309024/2s8YKFFMfy
