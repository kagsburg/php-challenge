<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //add the multiple statuses
        $statuses = [
            'awaiting-start',
            'in-progress',
            'on-hold',
            'complete',
        ];
        //loop through the statuses and add them to the database
        foreach ($statuses as $status) {
            \App\Models\ProjectStatus::create(['name' => $status]);
        }
    }
}
