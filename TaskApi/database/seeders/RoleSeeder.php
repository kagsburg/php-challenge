<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //add the multiple roles
        $roles = [
            'Project Manager',
            'Engineer',
            'Designer',
        ];
        //loop through the roles and add them to the database
        foreach ($roles as $role) {
            \App\Models\Role::create(['name' => $role]);
        }
    }
}
