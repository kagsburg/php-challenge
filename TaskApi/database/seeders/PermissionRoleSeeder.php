<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //assign the permissions to the roles
        $role = \App\Models\Role::where('name', 'Project Manager')->first();
        $role->permissions()->sync([
            \App\Models\Permission::where('name', 'permission-view')->first()->id,
            \App\Models\Permission::where('name', 'permission-create')->first()->id,
            \App\Models\Permission::where('name', 'permission-edit')->first()->id,
            \App\Models\Permission::where('name', 'permission-delete')->first()->id,
            \App\Models\Permission::where('name', 'permission-update')->first()->id,
            \App\Models\Permission::where('name', 'permission-set-complete')->first()->id,
        ]);
        $role = \App\Models\Role::where('name', 'Engineer')->first();
        $role->permissions()->sync([
            \App\Models\Permission::where('name', 'permission-view')->first()->id,
            \App\Models\Permission::where('name', 'permission-create')->first()->id,
            \App\Models\Permission::where('name', 'permission-edit')->first()->id,
            \App\Models\Permission::where('name', 'permission-update')->first()->id,           
        ]);
    }
}
