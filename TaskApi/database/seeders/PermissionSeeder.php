<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //add the multiple permissions
        $permissions = [
            'permission-view',
            'permission-create',
            'permission-edit',
            'permission-delete',
            'permission-update',
            'permission-set-complete'
        ];
        //loop through the permissions and add them to the database
        foreach ($permissions as $permission) {
            \App\Models\Permission::create(['name' => $permission]);
        }
        
    }
}
