<x-layout>
    <div class="container mx-auto">
        {{-- Back to home --}}
        <a href="/" class="text-black
            text-sm font-bold mb-2 inline-block
            border border-black rounded py-2 px-4
            hover:bg-black hover:text-white
        ">
            Back to home
        </a>
        <div class="lg:grid lg:grid-cols-2 gap-4 space-y-4 md:space-y-0 mx-4">
        {{-- list the series of Unique words --}}
        <div class="mt-6">
            <h1 class="text-2xl font-bold mb-6">Unique Words</h1>
            <ul>
                @foreach ($unique as $item)
                    <li class="text-lg">{{$item}}</li>
                @endforeach
            </ul>
        </div>
        {{-- list the series of Puncation marks --}}
        <div class="mt-6">
            <h1 class="text-2xl font-bold mb-6">Puncation Marks</h1>
            <ul>
                @foreach ($puncation as $item)
                    <li class="text-lg">{{$item}}</li>
                @endforeach
            </ul>
        </div>
        </div>
    </div>
</x-layout>