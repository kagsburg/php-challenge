

{{-- get data from trial controller --}}
<x-layout>

    <div class="mx-4">
        <x-card
            class="p-10 max-w-lg mx-auto mt-24"
        >
            <header class="text-center">
                <h2 class="text-2xl font-bold uppercase mb-1">
                    PHP Challenge Questions
                </h2>
                <p class="text-sm text-gray-500">
                    Please answer the following questions
                </p>
            </header>
            <form method="POST" action="/fibonacci" enctype="multipart/form-data">
                @csrf
                <div class="mt-6">
                    <label for="name" class="block text-sm font-bold mb-2">
                        Question 1 (Fibonacci Series)- Insert range of numbers
                    </label>
                    <input
                        type="text"
                        name="name"
                        id="name"
                        placeholder="Enter range of numbers e.g 1-10"
                        class="w-full border border-gray-300 p-2 rounded"
                        value="{{old('name')}}"
                    />
                    @error('name')
                        <p class="text-red-500 text-xs mt-1">{{$message}}</p>
                    @enderror
                </div>
                {{-- submit  --}}
                <div class="mt-6">
                    <button
                        type="submit"
                        class="bg-laravel text-white text-sm uppercase font-bold py-2 px-4 rounded hover:bg-laravel-dark"
                    >
                        Submit
                    </button>
            </form>
            <form method="POST" action="/contact" enctype="multipart/form-data">
                @csrf
                <div class="mt-6">
                    <label for="" class="block text-sm font-bold mb-2">
                        Question 2 (Get contact Info)- Insert Paragraph.
                    </label>
                    <textarea
                    class="border border-gray-200 rounded p-2 w-full"
                    name="paragraph"
                    rows="10" 
                    placeholder="Enter Paragraph"
                        >
                        {{old('paragraph')}}
                    </textarea>
                    @error('paragraph')
                        <p class="text-red-500 text-xs mt-1">{{$message}}</p>
                    @enderror
                </div>
                {{-- submit  --}}
                <div class="mt-6">
                    <button
                        type="submit"
                        class="bg-laravel text-white text-sm uppercase font-bold py-2 px-4 rounded hover:bg-laravel-dark"
                    >
                        Submit
                    </button>
            </form>
            <form method="POST" action="/unique" enctype="multipart/form-data">
                @csrf
                <div class="mt-6">
                    <label for="textfile" class="block text-sm font-bold mb-2">
                        Question 3 (Text File)- Browse file.
                    </label>
                    <input
                    type="file"
                    class="border border-gray-200 rounded p-2 w-full"
                    name="textfile"
                />
                @error('textfile')
                <p class="text-red-500 text-xs mt-1">{{$message}}</p>
                    
                @enderror
                </div>
                {{-- submit  --}}
                <div class="mt-6">
                    <button
                        type="submit"
                        class="bg-laravel text-white text-sm uppercase font-bold py-2 px-4 rounded hover:bg-laravel-dark"
                    >
                        Submit
                    </button>
            </form>
        </x-card>
    </div>
    {{-- <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>
    
                    <div class="panel-body">
                        <h1>Test</h1>
                        <p>{}</p>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
</x-layout>
    