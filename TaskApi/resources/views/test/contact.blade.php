<x-layout>
    <div class="container mx-auto">
        {{-- Back to home --}}
        <a href="/" class="text-black
            text-sm font-bold mb-2 inline-block
            border border-black rounded py-2 px-4
            hover:bg-black hover:text-white
        ">
            Back to home
        </a>
        {{-- list the contact details using Regex --}}
        <div class="lg:grid lg:grid-cols-2 gap-4 space-y-4 md:space-y-0 mx-4">
        <div class="mt-6">
            <h1 class="text-2xl font-bold mb-6">Contact Details Using Regex</h1>
              <div>
                <h2 class="text-lg font-bold mb-2">Email Address</h2>
                @foreach($withRegex['email'] as $email)
                    <p class="text-lg">{{$email}}</p>
                @endforeach
                </div>
                <div>
                <h2 class="text-lg font-bold mb-2">Phone Number</h2>
                @foreach($withRegex['phone'] as $phone)
                    <p class="text-lg">{{$phone}}</p>
                @endforeach
                </div>
                <div>
                <h2 class="text-lg font-bold mb-2">URL</h2>
                @foreach($withRegex['url'] as $url)
                    <p class="text-lg">{{$url}}</p>
                @endforeach
                </div>

        </div>
        {{-- list contact details without Regex --}}
        <div class="mt-6">
            <h1 class="text-2xl font-bold mb-6">Contact Details without Using Regex</h1>
              <div>
                <h2 class="text-lg font-bold mb-2">Email Address</h2>
                @foreach($withoutRegex['email'] as $email)
                    <p class="text-lg">{{$email}}</p>
                @endforeach
                </div>
                <div>
                <h2 class="text-lg font-bold mb-2">Phone Number</h2>
                @foreach($withoutRegex['phone'] as $phone)
                    <p class="text-lg">{{$phone}}</p>
                @endforeach
                </div>
                <div>
                <h2 class="text-lg font-bold mb-2">URL</h2>
                @foreach($withoutRegex['url'] as $url)
                    <p class="text-lg">{{$url}}</p>
                @endforeach
                </div>

        </div>
        </div>
    </div>
</x-layout>