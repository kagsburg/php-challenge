<x-layout>
    <div class="container mx-auto">
        {{-- Back to home --}}
        <a href="/" class="text-black
            text-sm font-bold mb-2 inline-block
            border border-black rounded py-2 px-4
            hover:bg-black hover:text-white
        ">
            Back to home
        </a>
        {{-- list the series of fibonaci number --}}
        <div class="mt-6">
            <h1 class="text-2xl font-bold mb-6">Fibonacci Series</h1>
            <ul>
                @foreach ($series as $item)
                    <li class="text-lg">{{$item}}</li>
                @endforeach
            </ul>
        </div>
    </div>
</x-layout>