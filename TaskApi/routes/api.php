<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProjectController;
use App\Http\COntrollers\AuthController;
use App\Http\Controllers\MilestoneController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//open routes
Route::prefix('v1')->group(function () {
    Route::post('/register',[UserController::class,'Register']);
    Route::post('/signin',[AuthController::class,'login']);
    Route::get('/roles',[UserController::class,'getRoles']);
});


Route::middleware('auth:sanctum')->group(function (){
//api prefix for all routes
Route::prefix('v1')->group(function () {
    // user routes
    Route::apiResource('users', UserController::class);
    Route::apiResource('projects', ProjectController::class);
    Route::get('/projects/user/{id}',[ProjectController::class,'getProjects']);
    Route::patch('/projects/changeMembers/{id}',[ProjectController::class,'changeMembers']);
    Route::post('/milestones/{id}',[MilestoneController::class,'addMilestone']);
    Route::get('/milestones/{id}',[MilestoneController::class,'getMilestones']);
    Route::apiResource('milestones', MilestoneController::class);
});
});
