<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[App\Http\Controllers\TrialController::class, 'index']);
Route::post('/fibonacci',[App\Http\Controllers\TrialController::class, 'fibonacci']);
Route::post('/contact',[App\Http\Controllers\TrialController::class, 'contact']);
Route::post('/unique',[App\Http\Controllers\TrialController::class, 'unique']);

