<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'start_date',
        'end_date',
        'project_status_id',
    ];

    public function projectStatus()
    {
        return $this->belongsTo(ProjectStatus::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
