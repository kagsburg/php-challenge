<?php

namespace App\Services;

use App\Http\Resources\RoleCollection;
use App\Http\Resources\UserCollection;
use App\Models\User;
use App\Models\Role;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class UserService
{
    public function index()
    {
        return new UserCollection(User::paginate());
    }
    
    public function storeUser(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|max:255|Email|unique:users',
            // password must be at least 8 characters 
            'password' => 'required|min:8',
            'password' => 'required|',
            'role' => 'required',
        ]);
 
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role_id' => $request->role,
        ]);
        //return using the resource
        return (new UserResource($user))->response()->setStatusCode(Response::HTTP_CREATED);
        
    }

    //get all Roles
    public function getRoles()
    {
        return new RoleCollection(Role::paginate()) ;
    }
}