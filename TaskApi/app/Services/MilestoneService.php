<?php

namespace App\Services;

use App\Http\Resources\MilestoneCollection;
use App\Models\Project;
use App\Models\Milestone;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\MilestoneResource;


class MilestoneService
{
    public function index()
    {
        return new MilestoneCollection(Milestone::paginate()) ;
    }

    public function store(Request $request, $id)
    {   
        //validate request
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'start' => 'required| date',
            'end' => 'required| date',            
        ]);
 
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 403);
        }
        //check if the project exists
        $project = Project::find($id);
        if(!$project){
            return response()->json(['error'=>'Project does not exist'], 403);            
        }

        $milestone = Milestone::create([
            'title' => $request->title,
            'description' => $request->description,
            'project_id' => $id,
            'start_date' => $request->start,
            'end_date' => $request->end,
        ]);
         //return using the resource
         return (new MilestoneResource($milestone))->response()->setStatusCode(Response::HTTP_CREATED);

    }
    //get all milestones for a project
    public function getMilestones(Request $request ,$id)
    {
        //check if the project exists
        $project = Project::find($id);
        if(!$project){
            return response()->json(['error'=>'Project does not exist'], 403);            
        }
        //get all milestones for the project
        $milestones = Milestone::where('project_id', $id)->get();
        return new MilestoneCollection($milestones);
    }
    public function update(Request $request, $id)
    {
        $milestone = Milestone::find($id);
        $milestone->name = $request->name;
        $milestone->description = $request->description;
        $milestone->project_id = $request->project_id;
        $milestone->save();
        return $milestone;
    }

    
}