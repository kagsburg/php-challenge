<?php


namespace App\Services;



use App\Http\Resources\ProjectCollection;
use App\Models\Project;
use App\Models\User;
use App\Models\ProjectStatus;
use Laravel\Sanctum\PersonalAccessToken;
use App\Http\Resources\ProjectResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ProjectService
{
    public function index()
    {
        return new ProjectCollection(Project::paginate());
    }
    
    public function storeProject(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'description' => 'required|max:255',
            'start' => 'required',
            'end' => 'required',            
        ]);
 
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 403);
        }
        //check if members are assigned to the project
        if(!$request->members){
            return response()->json(['error'=>'Members are required'], 403);            
        }
        //check if the among the members assigned to the project, atleast one project manager and one engineer
        $projectManager = false;
        $engineer = false;
        foreach ($request->members as $member) {
            //check if the member is a project manager
            $role = User::find($member)->role;
            if($role->name == 'Project Manager'){
                $projectManager = true;
            }
            //check if the member is an engineer
            if($role->name == 'Engineer'){
                $engineer = true;
            }
        }
        if(!$projectManager){
            return response()->json(['error'=>'Atleast one project manager is required'], 403);            
        }
        if(!$engineer){
            return response()->json(['error'=>'Atleast one engineer/developer is required'], 403);            
        }
        //check if the project start date is not before today
        if($request->start < date('Y-m-d')){
            return response()->json(['error'=>'Project start date cannot be before today'], 403);            
        }
        //check if the project end date is not before the start date
        if($request->end < $request->start){
            return response()->json(['error'=>'Project end date cannot be before the start date'], 403);            
        } 
        //status is set to awaiting start by default
        $status = ProjectStatus::where('name', 'awaiting-start')->first();
        $project = Project::create([
            'title' => $request->name,
            'description' => $request->description,
            'project_status_id' => $status->id,
            'start_date' => $request->start,
            'end_date' => $request->end,
        ]);
        //attach members to the project
        $project->users()->attach($request->members);
         
        //return using the resource
        return (new ProjectResource($project))->response()->setStatusCode(Response::HTTP_CREATED);
        
    }


    public function show($id)
    {
        return Project::find($id);
    }
    //update project details    
    public function update(Request $request, $id)
    {
        //get landlord id from token 
        $token = PersonalAccessToken::findToken($request->bearerToken());
        $user= $token->tokenable; 
        $project = Project::find($id);
        //check if the request is to update the project status  only
        if($request->status){
            //check if the project is completed
            if($project->projectStatus->name == 'complete'){
                //if user's role is engineer, return error
                if($user->role->name == 'Engineer'){
                    return response()->json(['error'=>'You cannot update an Already Completed project'], 403);            
                }            
            }
            //check if the project status is valid
            $status = ProjectStatus::where('id', $request->status)->first();
            if(!$status){
                return response()->json(['error'=>'Invalid project status'], 403);            
            }
            //check if the project status is awaiting start
            if($status->name == 'awaiting-start'){
                return response()->json(['error'=>'Project status cannot be set to awaiting start'], 403);            
            }
            //check if the project status is completed
            if($status->name == 'complete'){
                //check if the user's role has the permission to complete a project
                if(!$user->role->permissions->contains('name', 'permission-set-complete')){
                    return response()->json(['error'=>'You do not have permission to complete a project'], 403);            
                }
                //check if the project end date is not before today
                if($project->end_date < date('Y-m-d')){
                    return response()->json(['error'=>'Project end date cannot be before today'], 403);            
                }
            }
            // //update the project status
            // $project->project_status_id = $status->id;
            // $project->save();
            // return (new ProjectResource($project))->response()->setStatusCode(Response::HTTP_OK);
        }
        
        $project->update($request->all());
        return (new ProjectResource($project))->response()->setStatusCode(Response::HTTP_ACCEPTED);
    }
    //get projects assigned to an engineer
    public function getEngineerProjects(Request $request, $id){
        //get engineer id from token 
        $token = PersonalAccessToken::findToken($request->bearerToken());
        $user= $token->tokenable; 
        //check if the engineer id in the url is the same as the engineer id in the token
            // if($user->id != $id){
            //     return response()->json(['error'=>'You do not have permission to view this engineer\'s projects'], 403);            
            // }
        //get the engineer's projects
        $projects = Project::whereHas('users', function($q) use ($id){
            $q->where('user_id', $id);
        })->get();
        return new ProjectCollection($projects);
    }
    //change the users assigned to a project
    public function changeProjectMembers(Request $request, $id){
        //get landlord id from token 
        $token = PersonalAccessToken::findToken($request->bearerToken());
        $user= $token->tokenable;         
        //check if members are assigned to the project
        if(!$request->members){
            return response()->json(['error'=>'Members are required'], 403);            
        }
        //check if the among the members assigned to the project, atleast one project manager and one engineer
        $projectManager = false;
        $engineer = false;
        foreach ($request->members as $member) {
            $user = User::find($member);
            if($user->role->name == 'Project Manager'){
                $projectManager = true;
            }
            if($user->role->name == 'Engineer'){
                $engineer = true;
            }
        }
        if(!$projectManager){
            return response()->json(['error'=>'Atleast one project manager is required'], 403);            
        }
        if(!$engineer){
            return response()->json(['error'=>'Atleast one engineer is required'], 403);            
        }
        //get the project
        $project = Project::find($id);
        //detach all the users from the project
        $project->users()->detach();
        //attach the new users to the project
        $project->users()->attach($request->members);
        return (new ProjectResource($project))->response()->setStatusCode(Response::HTTP_ACCEPTED);

    }

    
}