<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TrialController extends Controller
{
    //
    //make a function to return an array of fibonacci numbers
    public function index(){
        return view('test.index');
    }
    public function fibonacci(Request $request)
    {
        //validate input
        $request->validate([
            //name should be required and contain only numbers and -
            'name' => 'required|regex:/^[\d-]+$/',
        ]);
        //get input
        $input = $request->input('name');
        //check if input contains -
        if(strpos($input, '-') == false){
            //if input does not contain - , return error
            return back()->withErrors('Input should contain -');            
        }
        //convert input to array
        $arr = explode("-", $input);
        //check if 
        $fibonacci = array();
        $n=range($arr[0],$arr[1]);
        //sort array in descending order
        rsort($n);
        //loop through the array and add the previous two numbers to get the next number
        for ($i = 6; $i < count($n); $i++) {
            $n[$i] = $n[$i - 1] + $n[$i - 2];
        }
        // return $fibonacci;
        //pass the array to the view
        return view('test.fibo',  ['series' => $n]);
    }
    public function contact(Request $request){
        //validate input
        $request->validate([
            //paragraph should be required and string
            'paragraph' => 'required|string',
        ]);
        //get input
        $input = $request->input('paragraph');
        $with = $this->getContact($input);
        // dd($with);

        //pass the array to the view
        return view('test.contact',  ['withRegex' => $with, 'withoutRegex' => $this->getContact2($input)]);
    }
    public function unique(Request $request){
        //validate input
        $request->validate([
            //textfile should be required and .txt
            'textfile' => 'required|mimes:txt',
        ]);
        //get text file
        $file = $request->file('textfile');

        $unqiue = $this->getUniqueWords($file);
        $puncation = $this->getPunctuation($file);
        // dd($unqiue, $puncation);
        //pass the array to the view
        return view('test.unique',  ['unique' => $unqiue, 'puncation' => $puncation]);
    }
     //function to return email , phone number and url from string using regex
     public function getContact($str){
        //create array to store email, phone number and url
        $arr = array();
        //add email to array
        $arr['email'] = $this->getEmail($str);
        //add phone number to array
        $arr['phone'] = $this->getPhone($str);
        //add url to array
        $arr['url'] = $this->getUrl($str);
        return $arr;
    }
    
    public function getEmail($str){
        //regex to match email
        $email = "/[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+/";
        //match email
        preg_match_all($email, $str, $matches);
        return $matches[0];
    }
    public function getPhone($str){
       //regex to match phone number
       $phone = "/\(?[0-9]{3}\)?[-. ]?[0-9]{3}[-. ]?[0-9]{6}/";
        //match phone number
        preg_match_all($phone, $str, $matches);
        return $matches[0];
    }
    public function getUrl($str){
         //regex to match url
        $url = "/(http|https):\/\/[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+/";
        //match url
        preg_match_all($url, $str, $matches);
        return $matches[0];
    }
    //function to return email , phone number and url from string without using regex
    public function getContact2($str){
        //create array to store email, phone number and url
        $arr = array();
        //create array to store email
        $arr['email'] = $this->getEmail2($str);
        //create array to store phone number
        $arr['phone'] = $this->getPhone2($str);
        //create array to store url
        $arr['url'] = $this->getUrl2($str);
        return $arr;
    }
    //function to get email without using regex
    public function getEmail2($str){
        //create array to store email
        $arr = array();
        //create array to store words in string
        $words = explode(" ", $str);
        //loop through words in string
        foreach($words as $word){
            //check if word contains @ and .
            if (strpos($word, '@') !== false && strpos($word, '.') !== false) {
                //check if word contains @ and . in the right order
                if(strpos($word, '@') < strpos($word, '.')){
                    //add word to array of email
                    $arr[] = $word;
                }
            }
        }
        return $arr;
    }
    //function to get phone number without using regex
    public function getPhone2($str){
        //create array to store phone number
        $arr = array();
        //create array to store words in string
        $words = explode(" ", $str);
        //loop through words in string
        foreach($words as $word){
            //check if word contains phone number
            if ((int)filter_var($word, FILTER_SANITIZE_NUMBER_INT) && strlen($word) >9) $arr[] = $word;             
        }
        return $arr;
    }
    //function to get url without using regex
    public function getUrl2($str){
        //create array to store url
        $arr = array();
        //create array to store words in string
        $words = explode(" ", $str);
        //loop through words in string
        foreach($words as $word){
            //check if word contains url
            if (filter_var($word, FILTER_VALIDATE_URL)) $arr[] = $word;            
        }
        return $arr;
    }
        //function to read text file and return array with Unique words
        public function getUniqueWords($file){
            //create array to store duplicate words
            $arr = array();
            //read file
            $str = file_get_contents($file);
            //create array to store words in file
            $words = explode(" ", $str);
           $unq = array_unique($words);
            return $unq;
        }
        //function to read text file and return array without duplicate words
        public function removeDuplicates($file){
            //create array to store punctuation marks
            $arr = array();
            //read file
            $str = file_get_contents($file);
            $word_split = explode(" ", $str);
           //loop through words in file
            foreach($word_split as $word){
                //check if word is repeated
                if (!(substr_count($str, $word) > 1)) {
                    //add word to array of duplicate words
                    $arr[] = $word;
                }
            }
            return $arr;
        }
        //function to return the punctuation marks in a string
        public function getPunctuation($str){
            //create array to store punctuation marks
            $arr = array();
            //read text file
            $text = file_get_contents($str);
            $punc = preg_split('/[[:punct:]]/', $text);
            //create array to store words in string
            $words_split = explode(" ", $text);
            //loop through words in string
            foreach($words_split as $word){
                //check if word contains punctuation marks
                if (preg_match('/[[:punct:]]/', $word, $matches))  $arr[] = $matches[0];
                   
            }
            return $arr;
        }
}
