<?php

namespace App\Http\Controllers;

use App\Models\User;

use App\Http\Resources\UserCollection;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Requests\StoreUserRequest;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Hash;
// use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }
    /**
     * Display all users.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->userService->index();
    }
    //store user
    public function Register (Request $request){
        return $this->userService->storeUser($request);
    }
    //get all roles
    public function getRoles(){
        return $this->userService->getRoles();
    }

}
