<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    //
    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        
        ]);
 
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $user =User::where('email',$request->email)->first();

        if(!$user ||!Hash::check($request->password,$user->password)){
            $response=[
                'statusCode'=>401,
                'message'=>'Invalid Password or Email',
                
            ];
            return response($response);
        }
        $token = $user->createToken($user->id)->plainTextToken;
        $response=[
            'status'=>'success',
            'user'=>new UserResource($user),
            'token'=>$token
        ];
        return response($response)->setStatusCode(Response::HTTP_OK);
    }
    public function logout(Request $request){
        if ($request->user()) { 
            $request->user()->tokens()->delete();
        }
        return response( [
            'message'=>'User Logged out'
        ],200);
    }
    public function validateApiToken(Request $request){
        return response(['message'=>'User Logged in'],200);
    }
}
