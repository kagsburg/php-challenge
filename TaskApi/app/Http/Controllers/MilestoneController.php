<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\MilestoneService;

class MilestoneController extends Controller
{
    //
    protected $milestoneService;

    public function __construct(MilestoneService $milestoneService)
    {
        $this->milestoneService = $milestoneService;
    }
    //get all milestones
    public function index()
    {
        return $this->milestoneService->index();
    }
    //create a milestone
    public function addMilestone(Request $request, $id)
    {
        return $this->milestoneService->store($request, $id);
    }
    //get all milestones for a project
    public function getMilestones(Request $request ,$id)
    {
        return $this->milestoneService->getMilestones($request, $id);
    }
}
