<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProjectService;

class ProjectController extends Controller
{
    //
    protected $projectService;

    public function __construct(ProjectService $projectService)
    {
        $this->projectService = $projectService;
    }

    public function index()
    {
        return $this->projectService->index();
    }

    //store a new project
    public function store(Request $request)
    {
        return $this->projectService->storeProject($request);
    }
    //update a project
    public function update(Request $request, $id)
    {
        return $this->projectService->update($request, $id);
    }
    //get projects assigned to a user
    public function getProjects(Request $request ,$id)
    {
        return $this->projectService->getEngineerProjects($request,$id);
    }
    //change members assigned to a project
    public function changeMembers(Request $request, $id)
    {
        return $this->projectService->changeProjectMembers($request, $id);
    }
}
