<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.
Laravel is accessible, powerful, and provides tools required for large, robust applications.

This repository contains the API for the PHP Challenge. The API is built using Laravel 9 and is a RESTful API.

## Installation

1. Clone the repository
2. Run `composer install`
3. Run `cp .env.example .env`
4. Run `php artisan key:generate`
5. Run `php artisan migrate --seed`
6. Run `php artisan serve`

## Usage

The API has the following endpoints:

- `POST /api/v1/register` - Register a new user
- `GET /api/v1/users` - Get all users
- `POST api/v1/signin` - Sign in a user
- `POST /api/v1/projects` - Create a new Project
- `GET /api/v1/projects` - Get all projects
- `GET /api/v1/projects/user/{id}` - Get a List of Projects Assigned to Specific Engineer
- `PATCH /api/v1/projects/{id}` - Update a project
- `GET /api/v1/roles` - Get all roles
- `PUT /api/v1/projects/changeMembers/{id}` - Update a Change Project Members
- `POST /api/v1/milestones/{id}` - Create a Milestone for a Project
- `GET /api/v1/milestones/{id}` - Get all Milestones for a Project
- `GET /api/v1/milestones` - Get all Milestones



