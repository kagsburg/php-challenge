<?php

require_once './setup/Quiz.php';

$quiz = new Quiz();


//create array of fibonacci numbers 0 to 100
$arr = range(0,100);
//call function to return array of fibonacci numbers
$Fibonacci = $quiz->getFibonacci($arr);
//print array of fibonacci numbers
echo "Fibonacci numbers: ";
print_r($Fibonacci);
echo "<br/> <br/>";
//create string
$paragraph= "This is a paragraph and it has to find 256781123456, 078945678347 , testemail@gmail.com and https://kanzucode.com/";
//call function to return email, phone number and url from string
$contactInfo = $quiz->getContactInfo($paragraph);
echo "Contact Info Using Regex :";
//print email, phone number and url
print_r($contactInfo);
echo "<br/> <br/>";
//call function to return email, phone number and url from string without using regex
$contactInfo2 = $quiz->getContactInfo2($paragraph);
echo "Contact Info Without Regex :";
//print email, phone number and url
print_r($contactInfo2);
echo "<br/> <br/>";
//call function to get duplicate words from file 
$words = $quiz->getUniqueWords('./test-file.txt');
echo "Unique Words :";
//print duplicate words
print_r($words);
echo "<br/> <br/>";
//call function to get all punctuation marks from file
$punctuation = $quiz->getPunctuation('./test-file.txt');
echo "Punctuation Marks :";
//print punctuation marks
print_r($punctuation);
echo "<br/> <br/>";

//call function to get all punctuation marks from file
$words = $quiz->removeDuplicates('./test-file.txt');
//print punctuation marks
print_r($words);