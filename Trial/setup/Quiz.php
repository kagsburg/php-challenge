<?php


//create class Quiz

class Quiz{

    
    //function to return array of fibonacci numbers
    public function getFibonacci($arr){
        //sort array in descending order
        rsort($arr);
        //start from the 7th element of the array
        for($i = 6; $i < count($arr); $i++){
            $arr[$i] = $arr[$i-1] + $arr[$i-2];
        }
        return $arr;
    }
    //function to return email , phone number and url from string using regex
    public function getContactInfo($str){
        //create array to store email, phone number and url
        $arr = array();
        //add email to array
        $arr['email'] = $this->getEmail($str);
        //add phone number to array
        $arr['phone'] = $this->getPhone($str);
        //add url to array
        $arr['url'] = $this->getUrl($str);
        return $arr;
    }
    
    public function getEmail($str){
        //regex to match email
        $email = "/[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+/";
        //match email
        preg_match_all($email, $str, $matches);
        return $matches[0];
    }
    public function getPhone($str){
       //regex to match phone number
       $phone = "/\(?[0-9]{3}\)?[-. ]?[0-9]{3}[-. ]?[0-9]{6}/";
        //match phone number
        preg_match_all($phone, $str, $matches);
        return $matches[0];
    }
    public function getUrl($str){
         //regex to match url
        $url = "/(http|https):\/\/[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+/";
        //match url
        preg_match_all($url, $str, $matches);
        return $matches[0];
    }
    //function to return email , phone number and url from string without using regex
    public function getContactInfo2($str){
        //create array to store email, phone number and url
        $arr = array();
        //create array to store email
        $arr['email'] = $this->getEmail2($str);
        //create array to store phone number
        $arr['phone'] = $this->getPhone2($str);
        //create array to store url
        $arr['url'] = $this->getUrl2($str);
        return $arr;
    }
    //function to get email without using regex
    public function getEmail2($str){
        //create array to store email
        $arr = array();
        //create array to store words in string
        $words = explode(" ", $str);
        //loop through words in string
        foreach($words as $word){
            //check if word contains @ and .
            if (strpos($word, '@') !== false && strpos($word, '.') !== false) {
                //check if word contains @ and . in the right order
                if(strpos($word, '@') < strpos($word, '.')){
                    //add word to array of email
                    $arr[] = $word;
                }
            }
        }
        return $arr;
    }
    //function to get phone number without using regex
    public function getPhone2($str){
        //create array to store phone number
        $arr = array();
        //create array to store words in string
        $words = explode(" ", $str);
        //loop through words in string
        foreach($words as $word){
            //check if word contains phone number
            if ((int)filter_var($word, FILTER_SANITIZE_NUMBER_INT) && strlen($word) >9) $arr[] = $word;             
        }
        return $arr;
    }
    //function to get url without using regex
    public function getUrl2($str){
        //create array to store url
        $arr = array();
        //create array to store words in string
        $words = explode(" ", $str);
        //loop through words in string
        foreach($words as $word){
            //check if word contains url
            if (filter_var($word, FILTER_VALIDATE_URL)) $arr[] = $word;            
        }
        return $arr;
    }
    //function to read text file and return array with Unique words
    public function getUniqueWords($file){
        //create array to store duplicate words
        $arr = array();
        //read file
        $str = file_get_contents($file);
        //create array to store words in file
        $words = explode(" ", $str);
       $unq = array_unique($words);
        return $unq;
    }
    //function to read text file and return array without duplicate words
    public function removeDuplicates($file){
        //create array to store punctuation marks
        $arr = array();
        //read file
        $str = file_get_contents($file);
        $word_split = explode(" ", $str);
       //loop through words in file
        foreach($word_split as $word){
            //check if word is repeated
            if (!(substr_count($str, $word) > 1)) {
                //add word to array of duplicate words
                $arr[] = $word;
            }
        }
        return $arr;
    }
    //function to return the punctuation marks in a string
    public function getPunctuation($str){
        //create array to store punctuation marks
        $arr = array();
        //read text file
        $text = file_get_contents($str);
        $punc = preg_split('/[[:punct:]]/', $text);
        //create array to store words in string
        $words_split = explode(" ", $text);
        //loop through words in string
        foreach($words_split as $word){
            //check if word contains punctuation marks
            if (preg_match('/[[:punct:]]/', $word, $matches))  $arr[] = $matches[0];
               
        }
        return $arr;
    }
    
}